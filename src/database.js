const mongoose = require('mongoose')

mongoose.connect(process.env.DB_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})
.then(connection => {
  console.log(`Connected to database ${ connection.connections[0].name }.`)
})
.catch(err => {
  console.error('Could not connect to database:', err)
})
