const express           = require('express')
const router            = express.Router()

const imageController   = require('./image/imageController')

// Index
router.get('/', (req, res) => { res.status(200).send('Welcome to Sauber.io') })

// Images
router.post('/images', imageController.create)
router.get('/images', imageController.readAll)
router.get('/images/:id', imageController.read)
router.delete('/images/:id', imageController.delete)

module.exports = router
