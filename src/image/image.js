const mongoose = require('mongoose')
const shortid = require('shortid')

const Schema = mongoose.Schema

module.exports = mongoose.model('image', new Schema({
  _id: {
    default: shortid.generate,
    type: String,
  },
  imageId: {
    required: true,
    type: String,
  },
  labelAnnotations: {
    type: Array,
  },
  logoAnnotations: {
    type: Array,
  },
  status: {
    default: 'P',
    required: true,
    type: String,
  }
}, {
  timestamps: true,
  toJSON: {
    getters: true,
  },
}))
