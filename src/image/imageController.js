const axios = require('axios')
const Image = require('./image')
const uploadcare = require('uploadcare')(process.env.UPLOADCARE_PUBLIC_KEY, process.env.UPLOADCARE_SECRET_KEY)

const { promisify } = require('util')
const uploadcareFileStore = promisify(uploadcare.files.store)
const uploadcareFileRemove = promisify(uploadcare.files.remove)

module.exports.create = async (req, res, next) => {
  try {
    if (!req.body.image_id) {
      return next('The form you submitted is incomplete.')
    }

    /**
     * Run through Google Vision API.
     * https://cloud.google.com/vision/docs/reference/rest/v1/images/annotate
     */
    try {
      var { data } = await axios.post(`https://vision.googleapis.com/v1/images:annotate?key=${ process.env.GOOGLE_API_KEY }`, {
        requests: [{
          image: {
            source: { imageUri: `https://ucarecdn.com/${ req.body.image_id }/` }
          },
          features: [
            { type: 'LABEL_DETECTION' },
            { type: 'LOGO_DETECTION' },
          ],
        }]
      })
    } catch(err) {
      return next(err.response.data.error)
    }

    /**
     * Save all obtained data in database.
     */
    var doc = new Image({
      'imageId': req.body.image_id,
      'status': 'A', // Consider the status as processed by Google.
    })

    if (data.responses[0].error) {
      return next(data.responses[0].error.message)
    }

    if (data.responses[0].labelAnnotations) { // LABEL_DETECTION
      doc.set('labelAnnotations', data.responses[0].labelAnnotations)
    }

    if (data.responses[0].logoAnnotations) { // LOGO_DETECTION
      doc.set('logoAnnotations', data.responses[0].logoAnnotations)
    }

    await doc.save()

    /**
     * Store the image on UploadCare.
     */
    await uploadcareFileStore(req.body.image_id)
  } catch(err) {
    return next(err)
  }

  return res.status(201).json(doc.toJSON())
}

module.exports.readAll = async (req, res, next) => {
  var docs = await Image.find().sort({ 'updatedAt': -1 })

  return res.status(200).json(docs.map(item => item.toJSON()))
}

module.exports.read = async (req, res, next) => {
  var doc = await Image.findOne({ '_id': req.params.id })

  if (!doc) {
    return next(null)
  }

  return res.status(200).json(doc.toJSON())
}

module.exports.delete = async (req, res, next) => {
  try {
    var doc = await Image.findOneAndRemove({ '_id': req.params.id })

    if (doc.imageId) {
      try {
        await uploadcareFileRemove(doc.imageId)
      } catch(err) {
        console.error(err.message, doc.imageId) // Don’t interrupt if cannot delete.
      }
    }
  } catch(err) {
    return next(err)
  }

  return res.status(200).json({
    id: doc.id,
    deleted: true,
  })
}
