const config = require('dotenv').config()

if (config.error) {
  console.error(config.error.message)
  process.exit()
}
