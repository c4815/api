const app           = require('express')()
const bodyParser    = require('body-parser')
const cors          = require('cors')
const helmet        = require('helmet')

if (app.get('env') !== 'production') {
  require('./config')
}

/**
 * Enable CORS from client-side.
 */
app.use(cors())

/**
 * Parse urlencoded bodies to JSON and expose the object in req.body when we
 * start building endpoints.
 */
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(bodyParser.text({ type: 'text/html' }))

/**
 * Protect from some well-known web vulnerabilities.
 */
app.use(helmet())

/**
 * Connect to the MongoDB database.
 */
require('./src/database')

/**
 * Serve the routes in router/index.js.
 */
app.use(require('./src/router'))

/**
 * Custom error handling for certain status codes.
 */
app.use((req, res, next) => {
  res.status(404).json({
    message: 'Endpoint does not exist.'
  })
})

app.use((err, req, res, next) => {
  console.error(new Date(), err)

  res.status(400).json({
    message: err.message || 'Internal error while trying to process your request.'
  })
})

/**
 * Start the Express server.
 */
app.listen(process.env.PORT || 8080, () => {
  console.log(`App listening in ${ app.get('env') }.`)
})
